/* 
 *  Pressure and Flow measurement for New Lung Function Test
 *  Author: Ted Lerios
 *  Date: 13 April 2021
 *  Sensor used: Custom built pressure and flow sensor by Lui Holder-Pearson (SDP816 MPVZ4006)
 *  
 *  
 */

#define LOOPDURATIONMICROS 2500

const int qVpin = A0; 
const int pVpin = A1; 

int pADC = 0;             
int qADC = 0;             

unsigned long t;
unsigned long tStart = 0;
unsigned long tNow = micros();
unsigned long tOver = 0;




void setup() {
  pinMode(qVpin, INPUT);
  pinMode(pVpin, INPUT);
  
  Serial.begin(115200);
}

void loop() {
  tStart = micros();
  

  // read the analog in value:
  pADC = analogRead(pVpin);
  qADC = analogRead(qVpin);

  if (t > LOOPDURATIONMICROS) {
    tOver = t - LOOPDURATIONMICROS;
  } else {
    tOver = 0;
  }
  
  // print the results to the Serial Monitor:
  Serial.print("t= ");
  Serial.print(t);
  Serial.print("\t p= ");
  Serial.print(pADC);
  Serial.print("\t q= ");
  Serial.println(qADC);

  tNow = micros();
  while((tNow - tStart) < (LOOPDURATIONMICROS - tOver)) {
    tNow = micros();
  }
  t = tNow - tStart;
  
}
