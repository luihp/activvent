#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

#define PCOLOUR 0xFFE0
#define QCOLOUR 0x07E0

#define BACKCOLOUR 0x0000

const bool usingJaycarScreen = 1;

/*
   Variables which are used for the grid scaling etc
*/


int curX = 0; // Current x position being graphed
uint8_t scale = 3; // Text scale factor

int potVal = 0;

const int button1Pin = 10;
const int button2Pin = 11;
const int button3Pin = 12;


enum SettingStates {
  choosing,
  changing,
  confirming
};

const byte numSubStates = 5;

//byte settingState = choosing;

enum State {
  plotting,
  settings,
  limits,
  source
};

uint8_t tLoop = 10;
unsigned long tStart = 0;
unsigned long loopC = 0;
unsigned long bgColour = BACKCOLOUR;


byte mode = plotting;

byte charIn = 0;
bool readData = 0;
bool readState = true;


unsigned long tLastPressed = 0;
const unsigned long tDeadband = 250; // Time in which it won't register two button presses
const unsigned long tHeld = 1000; // Time during which you have to hold it down to register a 'hold'

void setup()
{
  Serial.begin(115200);

  LCD_init();
  //delay(2000);
  LCD_clear(bgColour);
  //delay(2000);
  drawGrid();

  pinMode(button1Pin, INPUT_PULLUP);
  pinMode(button2Pin, INPUT_PULLUP);
  pinMode(button3Pin, INPUT_PULLUP);



  delay(1000);
  setDataStream(1);
  mode = 0;
}



int convAtoVal ( int aIn, int* fac ) {
  return ((aIn - fac[0]) * fac[1] / fac[2]);
}

void loop()
{
  tStart = millis();

  /*
    if (!digitalRead(button2Pin) &&
      (millis() - tLastPressed >= tDeadband)) {
    toggleDataStream();
    tLastPressed = millis();
    }
  */

  switch (mode / numSubStates) {
    case (plotting):
      plotLoop();

      if ((!digitalRead(button1Pin)) &&
          (millis() - tLastPressed >= tDeadband)) {
        mode = settings * numSubStates;
        tLastPressed = millis();
        initSettingsMode(true);
      }


      if ((!digitalRead(button2Pin)) &&
          (millis() - tLastPressed >= tDeadband)) {
        mode = source * numSubStates;
        tLastPressed = millis();
        initSourceMode();
      }
      break;



    case (settings):
      settingLoop();

      if (mode % numSubStates == choosing &&
          (!digitalRead(button1Pin)) &&
          (millis() - tLastPressed >= tDeadband)) {
        mode = limits * numSubStates;
        tLastPressed = millis();
        initScreenLimits(true);
      }
      break;


    case (limits):
      limitsLoop();

      if (mode % numSubStates == choosing &&
          (!digitalRead(button1Pin)) &&
          (millis() - tLastPressed >= tDeadband)) {

        mode = plotting * numSubStates;
        tLastPressed = millis();

        setDataStream(1);
        bgColour = BLACK;

        LCD_clear(bgColour);
        drawCleanGrid();
        curX = 0;
      }
      break;

    case (source):
      sourceLoop();

      
      if ((!digitalRead(button3Pin)) &&
          (millis() - tLastPressed >= tDeadband)) {
        mode = settings * numSubStates;
        tLastPressed = millis();
        initSettingsMode(true);
      }
  }

  loopC++;

}



uint8_t adcToLPM ( int valIn ) {
  int32_t val_mv = valIn * 190000 / 1023 - 38000;

  if (val_mv < 0) {
    return 0;
  }
  int32_t intermediateVal = sqrt(val_mv) * 2125 / 10000;

  return intermediateVal;
}
