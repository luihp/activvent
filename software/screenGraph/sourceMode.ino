uint8_t prevSourceSel = 0;
uint8_t sourceSel = 0;

void initSourceMode (void) {

  LCD_clear(bgColour);

  prevSourceSel = 10;
  sourceSel = analogRead(A7 / 256);
  showSourceScreen();

}


void sourceLoop ( void ) {
  switch ( mode % numSubStates ) {

    case choosing:

      sourceSel = analogRead(A7) / 256;

      if (!digitalRead(button2Pin) &&
          millis() - tLastPressed > tDeadband) {
        tLastPressed = millis();

        drawStr(0, 10 * scale * 5, WHITE, 3, "B2:confirm", 2);
        drawStr(0, 10 * scale * 6, WHITE, 3, "B3:cancel", 2);
        mode = source * numSubStates + confirming;

      }

      else if (sourceSel != prevSourceSel) {
        sourceScreenArrow();
        prevSourceSel = sourceSel;

      }
      break;

    case changing:
      break;

    case confirming:


      if (!digitalRead(button2Pin) &&
          millis() - tLastPressed > tDeadband + 500) {
        tLastPressed = millis();
        drawStr(0, 120, RED, scale, "SENDING", 7);
        setParam('g', sourceSel);

        delay(2000);


        mode = plotting * numSubStates;

        LCD_clear(bgColour);
        //delay(2000);
        drawGrid();


      } else

        if (!digitalRead(button3Pin) &&
            millis() - tLastPressed > tDeadband) {
          tLastPressed = millis();

          delay(50);

          LCD_clear(bgColour);
          //delay(2000);
          drawGrid();
          mode = plotting * numSubStates;
        }

      break;
  }

  while (millis() - tStart < tLoop) {
    ;
  }
}




void showSourceScreen ( void ) {
  sourceScreenText();
  sourceScreenArrow();
}

void sourceScreenArrow ( void ) {
  LCD_rect(198, 0, 55, 120, bgColour);
  LCD_rect(200, 10 + 10 * scale * sourceSel, 50, 5, WHITE);
  LCD_rect(198, 12 + 10 * scale * sourceSel, 2, 1, WHITE);
  LCD_rect(202, 7 + 10 * scale * sourceSel, 2, 11, WHITE);
}

void sourceScreenText( void ) {


  drawStr(0, 10 * scale * 0, MAGENTA, scale, "Insp", 2);
  drawStr(0, 10 * scale * 1, GREEN, 3, "Left", 2);
  drawStr(0, 10 * scale * 2, RED, 3, "Right:", 2);
  drawStr(0, 10 * scale * 3, WHITE, 3, "Exp:", 2);
  drawStr(0, 10 * scale * 5, WHITE, 3, "B2:sel", 2);

}
