
void drawStr( int16_t col, int16_t row, int16_t color, int8_t scale, byte *strIn, uint8_t len) {
  LCD_fancyMakeText(col, row, color, scale, strIn);
}

void drawInt( int16_t col, int16_t row, int16_t color, int8_t scale, int intIn, uint8_t len) {
  LCD_fancyMakeTextNum(col, row, color, scale, intIn);
}

/*
void drawStr( int16_t col, int16_t row, int16_t color, int8_t scale, byte *strIn, uint8_t len) {

  for (int i = 0; i < len; i++) {

    switch (strIn[i]) {
      case (' '):
        break;
      default:
        if (strIn[i] > ']') {
          letter(col + i * 6 * scale, row, color, scale, charChar(strIn[i] - 'a' + 'A'));
        }
        else if (strIn[i] > '<') {
          letter(col + i * 6 * scale, row, color, scale, charChar(strIn[i]));
        } else {
          letter(col + i * 6 * scale, row, color, scale, numChar(strIn[i]));
        }
    }
  }
}

void drawInt ( int16_t col, int16_t row, int16_t color, int8_t scale, int intIn, uint8_t len) {
  
  for (int i = 0; i < len; i++) {
    letter(col + (len-i - 1)*7*scale, row, color, scale, numChar((intIn %10) + '0'));
    intIn /= 10;
  }
}

void letter(int16_t col, int16_t row, uint16_t color, int8_t scale, byte *charStart) {
  
  int16_t width = 5 * scale;
  int16_t height = 8 * scale;
  
  LCD_setWindow(col, width, row, height);
  byte columnData;
  int i, j, k, l;

  byte flow = color >> 8;
  byte fhigh = color;

  byte blow = bgColour >> 8;
  byte bhigh = bgColour;

  //Serial.print("S");
  //Serial.println("");

  for (i = 0; i < width / scale; i++) {
    columnData = pgm_read_byte(&charStart[i]);
    for (k = 0; k < scale; k++) {
      //Serial.write(columnData);
      for (j = 0; j < height / scale; j++) {
        for (l = 0; l < scale; l++) {
          //Serial.println(j);
          if (columnData & (0x01 << j)) {
            LCD_data_write(flow);
            LCD_data_write(fhigh);
          } else {
            LCD_data_write(blow);
            LCD_data_write(bhigh);
          }
        }
      }

    }

  }
  
}

*/

/*

void letter(int16_t col, int16_t row, int16_t color, int8_t scale, byte *charStart) {


  int16_t width = 5 * scale;
  int16_t height = 8 * scale;

  
  LCD_command_write(0x2a); // Column Address Set
  LCD_data_write(row >> 8);
  LCD_data_write(row);
  LCD_data_write((row + height - 1) >> 8);
  LCD_data_write(row + height - 1);
  LCD_command_write(0x2b); // Page Address Set
  LCD_data_write(col >> 8);
  LCD_data_write(col);
  LCD_data_write((col + width - 1) >> 8);
  LCD_data_write(col + width - 1);
  LCD_command_write(0x2c); // Memory Write

  byte columnData;
  int i, j, k, l;

  byte flow = color >> 8;
  byte fhigh = color;

  byte blow = bgColour >> 8;
  byte bhigh = bgColour;

  //Serial.print("S");
  //Serial.println("");

  for (i = 0; i < width / scale; i++) {
    columnData = pgm_read_byte(&charStart[i]);
    for (k = 0; k < scale; k++) {
      //Serial.write(columnData);
      for (j = 0; j < height / scale; j++) {
        for (l = 0; l < scale; l++) {
          //Serial.println(j);
          if (columnData & (0x01 << j)) {

            LCD_data_write(flow);
            LCD_data_write(fhigh);
          } else {
            LCD_data_write(blow);
            LCD_data_write(bhigh);
          }

        }
      }

    }

  }
}
*/

const byte PROGMEM char1[] = {0x84, 0x82, 0xFF, 0x80, 0x80};
                             const byte PROGMEM char2[] = {0x82, 0xC1, 0xA1, 0x91, 0x8E};
                             const byte PROGMEM char3[] = {0x41, 0x89, 0x95, 0x93, 0x61};
                             const byte PROGMEM char4[] = {0x1F, 0x10, 0x10, 0xFF, 0x10};
                             const byte PROGMEM char5[] = {0x5F, 0x89, 0x89, 0x89, 0x71};
                             const byte PROGMEM char6[] = {0x7C, 0x92, 0x89, 0x89, 0x70};
                             const byte PROGMEM char7[] = {0x01, 0xF1, 0x09, 0x05, 0x03};
                             const byte PROGMEM char8[] = {0x76, 0x89, 0x89, 0x89, 0x76};
                             const byte PROGMEM char9[] = {0x06, 0x89, 0x89, 0x89, 0x7E};
                             const byte PROGMEM char0[] = {0x7E, 0xE1, 0x99, 0x87, 0x7E};
                             const byte PROGMEM char10[] = {0x00, 0x66, 0x66, 0x00, 0x00};
                             const byte PROGMEM charPlus[] = {0x10, 0x10, 0x7C, 0x10, 0x10};
                             const byte PROGMEM charMinus[] = {0x10, 0x10, 0x10, 0x10, 0x10};

byte* numChar( int in ) {
  in -= '0';
  switch (in) {
    case -5:
      return charPlus;
    case -3:
      return charMinus;
    case 0:
      return char0;
    case 1:
      return char1;
    case 2:
      return char2;
    case 3:
      return char3;
    case 4:
      return char4;
    case 5:
      return char5;
    case 6:
      return char6;
    case 7:
      return char7;
    case 8:
      return char8;
    case 9:
      return char9;
    case 10:
      return char10;
  }
}

const byte PROGMEM charA[] = {0xFE, 0x09, 0x09, 0x09, 0xFE};
                             const byte PROGMEM charB[] = {0xFF, 0x89, 0x89, 0x89, 0x7E};
                             const byte PROGMEM charC[] = {0x7E, 0x81, 0x81, 0x81, 0x42};
                             const byte PROGMEM charD[] = {0xFF, 0x81, 0x81, 0x81, 0x7E};
                             const byte PROGMEM charE[] = {0xFF, 0x89, 0x89, 0x89, 0x81};
                             const byte PROGMEM charF[] = {0xFF, 0x09, 0x09, 0x09, 0x01};
                             const byte PROGMEM charG[] = {0x7E, 0x81, 0x81, 0x89, 0x72};
                             const byte PROGMEM charH[] = {0xFF, 0x08, 0x08, 0x08, 0xFF};
                             const byte PROGMEM charI[] = {0x00, 0x81, 0xFF, 0x81, 0x00};
                             const byte PROGMEM charJ[] = {0x40, 0x80, 0x81, 0x7F, 0x01};
                             const byte PROGMEM charK[] = {0xFF, 0x08, 0x14, 0x22, 0xC1};
                             const byte PROGMEM charL[] = {0xFF, 0x80, 0x80, 0x80, 0x80};
                             const byte PROGMEM charM[] = {0xFF, 0x02, 0x0C, 0x02, 0xFF};
                             const byte PROGMEM charN[] = {0xFF, 0x06, 0x18, 0x60, 0xFF};
                             const byte PROGMEM charO[] = {0x7E, 0x81, 0x81, 0x81, 0x7E};
                             const byte PROGMEM charP[] = {0xFF, 0x09, 0x09, 0x09, 0x06};
                             const byte PROGMEM charQ[] = {0x7E, 0x81, 0xA1, 0x41, 0xBE};
                             const byte PROGMEM charR[] = {0xFF, 0x09, 0x19, 0x29, 0xC6};
                             const byte PROGMEM charS[] = {0x66, 0x89, 0x89, 0x91, 0x66};
                             const byte PROGMEM charT[] = {0x01, 0x01, 0xFF, 0x01, 0x01};
                             const byte PROGMEM charU[] = {0x7F, 0x80, 0x80, 0x80, 0x7F};
                             const byte PROGMEM charV[] = {0x3F, 0x40, 0x80, 0x40, 0x3F};
                             const byte PROGMEM charW[] = {0x7F, 0x80, 0x70, 0x80, 0x7F};
                             const byte PROGMEM charX[] = {0xC7, 0x28, 0x10, 0x28, 0xC7};
                             const byte PROGMEM charY[] = {0x07, 0x18, 0xE0, 0x18, 0x07};
                             const byte PROGMEM charZ[] = {0x81, 0xE1, 0x99, 0x87, 0x81};

byte * charChar ( int intIn ) {

  intIn -= 'A';
  switch (intIn) {
    case 0:
      return charA;
    case 1:
      return charB;
    case 2:
      return charC;
    case 3:
      return charD;
    case 4:
      return charE;
    case 5:
      return charF;
    case 6:
      return charG;
    case 7:
      return charH;
    case 8:
      return charI;
    case 9:
      return charJ;
    case 10:
      return charK;
    case 11:
      return charL;
    case 12:
      return charM;
    case 13:
      return charN;
    case 14:
      return charO;
    case 15:
      return charP;
    case 16:
      return charQ;
    case 17:
      return charR;
    case 18:
      return charS;
    case 19:
      return charT;
    case 20:
      return charU;
    case 21:
      return charV;
    case 22:
      return charW;
    case 23:
      return charX;
    case 24:
      return charY;
    case 25:
      return charZ;
  }
}
/*

  const byte charChar2[16] = {charN, charO, charP,charQ, charR, charS, charT, charU,
  charV, charW, charX, charY,  charZ};

  const byte charChar[16] = {
  charA, charB, charC, charD, charE, charF, charG,charH, charI, charJ, charK, charL, charM};
*/
