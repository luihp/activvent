int pFac[3] = {60, 5, 64}; // A very rough conversion is P(cmH2O) = ((P_adcCount - 60) / 16)
int qFac[3] = {195, 12, 50};

const uint8_t qSpan = 60;
const uint8_t pSpan = 40;

const uint8_t qPx = 100;
const uint8_t pPx = 100;

const uint8_t qZero = 230;
const uint8_t pZero = 110;

const uint8_t yStart = 8;
uint8_t xLen = (320 - yStart) / 2;





bool plottingCurVal = false;

const uint8_t bSpace = 20;

int pIn = 0;
int lastp = 0;
int qIn = 0;
int lastq = 0;

// These ones are for the current ones
int peepVals[2] = {0, 0};
int pipVals[2] = {0, 0};
unsigned long volVals[2] = {0, 0};

// These ones are for the last ones
int lastPeep[2] = {0, 0};
int lastPip[2] = {0, 0};
int lastVol[2] = {0, 0};

// These ones are for the prev ones
int prevPeep[2] = {0, 0};
int prevPip[2] = {0, 0};
int prevVol[2] = {0, 0};
bool cState = 0;

void showStats( void ) {

  
  if (cState) {
    LCD_rect(260, 55, 34, 50, bgColour);
    drawInt(260, 55, MAGENTA, 3, lastPip[cState], 2);
    drawInt(260, 80, YELLOW, 3, lastPeep[cState], 2);

    LCD_rect(260, pZero + 30, 50, 25, bgColour);
    drawInt(260, pZero + 30, GREEN, 3, lastVol[cState], 3);
  } else {


    LCD_rect(260, 0, 50, 25, bgColour);
    drawInt(260, 0, MAGENTA, 3, lastPip[cState], 2);
    drawInt(260, 25, YELLOW, 3, lastPeep[cState], 2);
    
    LCD_rect(260, pZero + 5, 50, 25, bgColour);
    drawInt(260, pZero + 5, GREEN, 3, lastVol[cState], 3);
    
  }
}

void checkStats( void ) {
  int curP = convAtoVal(lastp, pFac);

  peepVals[cState] = ( curP < peepVals[cState]) ? curP : peepVals[cState];
  pipVals[cState] = ( curP > pipVals[cState]) ? curP : pipVals[cState];
  volVals[cState] += (lastq);

}

void updateVals( void ) {
  prevPeep[0] = lastPeep[0];
  prevPeep[1] = lastPeep[1];

  lastPeep[0] = peepVals[0];
  lastPeep[1] = peepVals[1];


  prevPip[0] = lastPip[0];
  prevPip[1] = lastPip[1];

  lastPip[0] = pipVals[0];
  lastPip[1] = pipVals[1];

  prevVol[0] = lastVol[0];
  prevVol[1] = lastVol[1];

  lastVol[0] = volVals[0] * 2 / 3;
  lastVol[1] = volVals[1] * 2 / 3;

  peepVals[0] = 100;
  peepVals[1] = 100;

  pipVals[0] = 0;
  pipVals[1] = 0;

  volVals[0] = 0;
  volVals[1] = 1;

}

void plotLoop ( void ) {
  if (readData) {
    LCD_plot( pIn, qIn);

    if (plottingCurVal) {
      drawInt(20, 80, YELLOW, 3, convAtoVal(pIn, pFac), 2);
      drawInt(20, 200, GREEN, 3, adcToLPM(qIn), 2);


    }
    checkStats();
    readData = false;
  }

  if ((!digitalRead(button3Pin)) &&
      (millis() - tLastPressed >= tDeadband)) {
    tLastPressed = millis();


    plottingCurVal = !plottingCurVal;

    if ( plottingCurVal ) {
      LCD_rect(260, 0, 60, 240, bgColour);
      xLen = (320 - yStart - 80) / 2;
    } else {
      xLen = (320 - yStart) / 2;
    }
  }




  while (!readData && millis() - tStart < tLoop) {
    if (Serial.available() >= 5 ) {
      charIn = Serial.read();
      if (charIn == 'd') {
        pIn = 0;
        qIn = 0;

        pIn += Serial.read() << 8;
        pIn += Serial.read();

        qIn += Serial.read() << 8;
        qIn += Serial.read();
        readData = true;
      } else if (charIn == 'c') {

        LCD_rect(curX * 2 + yStart, 0, 2, 320,  MAGENTA);
        updateVals();

        if (plottingCurVal) {
          showStats();
        }
        cState = !cState;
      }

    }
  }
}



void LCD_plot ( int p, int q) {
  q = adcToLPM ( q );

  LCD_rect(curX * 2 + yStart, pZero - convAtoVal(((p < lastp) ? lastp : p), pFac), 2, convAtoVal(abs(p - lastp) + pFac[0], pFac) + 2, PCOLOUR);
  LCD_rect(curX * 2 + yStart  , qZero - ((q < lastq) ? lastq : q) , 2 , abs(q - lastq)  + 2 , QCOLOUR);

  LCD_rect((curX * 2 + bSpace % xLen), 0, 2, pZero, bgColour);
  LCD_rect((curX * 2 + bSpace % xLen), (pZero < qZero) ? pZero + 2 : qZero + 2, 2, (pZero < qZero) ? qZero - pZero - 2 : pZero - qZero - 2, BLACK);
  LCD_rect((curX * 2 + bSpace % xLen), (pZero > qZero) ? pZero + 2 : qZero + 2, 2, (pZero > qZero) ? 240 - pZero : 240 - qZero, BLACK);

  curX++;

  if (curX >= xLen) {
    drawCleanGrid();
    curX = 0;
  }

  lastp = p;
  lastq = q;
}

int myAbsDiff( int a, int b) {
  return (a > b ? a - b : b - a);
}


void drawCleanGrid( void ) {
  LCD_rect(0, 0, 22, 240, BLACK);
  drawGrid();
}

void drawGrid ( void ) {
  LCD_rect(yStart, 0, 2, 240, BLUE);
  LCD_rect(4, pZero, xLen * 2+(yStart - 4), 2, PCOLOUR);
  LCD_rect(4, qZero, xLen * 2+(yStart - 4), 2, QCOLOUR);

  for (int i = 1; i <= pSpan / 10; i++) {
    LCD_rect(4, pZero - (i * 10 * pPx / pSpan), 8, 2, CYAN);
  }
  for (int i = 1; i <= qSpan / 10; i++) {
    LCD_rect(4, qZero - (i * 10 * qPx / qSpan), 8, 2, CYAN);
  }
}
