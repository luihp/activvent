
unsigned long serialTimeout = 2500;
unsigned long tSerialWrite = 0;

bool dataStreamStatus = 1;


// Data stream stuff
void setDataStream ( bool state ) {
  dataStreamStatus = state;
  Serial.print("s");
  Serial.print(dataStreamStatus);
  digitalWrite(13, dataStreamStatus);
}

void toggleDataStream( void ) {
  setDataStream (!dataStreamStatus);
}


void clearSerial ( void ) {
  while (Serial.available()) {
    Serial.read();
    delay(1);
  }
}

int readLimit( char ch ) {
  return(readVal('l', ch));
}

void setLimit ( char ch, int val ) {
  delay(1);
  Serial.print('l');
  Serial.print(ch);
  Serial.print(val);

  delay(5);
}

void setParam( char ch, int val ) {
  Serial.print(ch);
  Serial.print(val);

  delay(5); // Just give enough time to purge/ignore the reply
}

// Reading parameters
int readParam( char charIn ) {
  return(readVal('q', charIn));
}

int readVal(char cha, char chb) {
  tSerialWrite = millis();
  Serial.print(cha);
  Serial.print(chb);
  delay(1);
  
  while ((!Serial.available()) &&
         (millis() - tSerialWrite < serialTimeout)) {
    ;
  }

  if (Serial.available()) {
    delay(1);
    return getSerialNumASCII();
  }

}
  



int getSerialNumASCII ( void ) {
  int in = 0;

  do {
    in *= 10;
    in += Serial.read() - '0';
  } while (Serial.available());

  return in;
}
