int curDelay = 0;
int curTol = 1000;
int curWatchdog = 0;
bool curPressIgnore = false;

int newVal = 0;
int oldVal = 0;
int diff = 0;

const int settingMins[3] = {0, 1000, 0};
const int settingMaxs[3] = {2000, 9500, 1};
const int settingPotSpan[3] = {1023, 3069, 1};
const int settingButtonDiff[3] = {50, 500, 1};
const char settingChar[3] = {'d', 'w', 'i'};

unsigned long tLastRead = 0;

enum VariableSelected {
  del,
  wdog,
  pig
};

byte valToChange = del;
byte prevValToChange = del;



void initSettingsMode (bool stopReading) {
  
  LCD_clear(0x00);
  
  if (stopReading) {
  setDataStream ( false );
  drawStr(0, 10 * scale * del, MAGENTA, scale, "Stopping data", 13);
  delay(2000);
  }
  drawStr(0, 10 * scale * wdog, MAGENTA, scale, "Reading data", 12);
  
  clearSerial();
  readAllparams();

  LCD_clear(0x00);
  prevValToChange = -1;

  //mode = settings * numSubStates + choosing;
  showReadScreen();
}



void settingLoop (void) {

  switch ( mode % numSubStates ) {
    case choosing:

      valToChange = analogRead(A7) / 341;

      if (!digitalRead(button2Pin) &&
          millis() - tLastPressed > tDeadband) {
        tLastPressed = millis();
        
        mode = settings * numSubStates + changing;
        
        potVal = analogRead(A7);
        diff = 0;
        oldVal = newVal;
        
        showNewVal();

        drawStr(0, 180, BLUE, scale, "b3: +", 5);
        drawStr(0, 210, BLUE, scale, "b1: -", 5);
      }


      if (valToChange != prevValToChange) {
        settingScreenArrow();
        prevValToChange = valToChange;

        switch ( valToChange ) {
          case del:
            newVal = curDelay;
            break;
          case wdog:
            newVal = curWatchdog;
            break;
          case pig:
            newVal = curPressIgnore;
            break;
        }
      }
      break;

    case changing:

      if (!digitalRead(button1Pin) &&
          millis() - tLastPressed > tDeadband) {
        tLastPressed = millis();
        diff -= settingButtonDiff[valToChange];
        checkLim();

        newVal = oldVal + diff;

        showNewVal();
      }
      if (!digitalRead(button3Pin) &&
          millis() - tLastPressed > tDeadband) {
        tLastPressed = millis();
        diff += settingButtonDiff[valToChange];
        checkLim();

        newVal = oldVal + diff;

        showNewVal();
      }
      if (!digitalRead(button2Pin) &&
          millis() - tLastPressed > tDeadband) {
        tLastPressed = millis();
        mode = settings * numSubStates + confirming;

        drawStr(0, 180, BLUE, scale, "b2: CONFIRM", 11);
        drawStr(0, 210, BLUE, scale, "b3: cancel", 10);
      }
      break;

    case confirming:


      if (!digitalRead(button2Pin) &&
          millis() - tLastPressed > tDeadband + 1000) {
        tLastPressed = millis();
        drawStr(0, 120, WHITE, scale, "SENDING", 7);
        setParam(settingChar[valToChange], newVal);
        
        delay(2000);

        initSettingsMode(false);

        mode = settings * numSubStates + choosing;
      }

      if (!digitalRead(button3Pin) &&
          millis() - tLastPressed > tDeadband) {
        tLastPressed = millis();

        delay(50);
        mode = settings * numSubStates + choosing;
        initSettingsMode(false);
      }

      break;
  }

  while (millis() - tStart < tLoop) {
    ;
  }
}

void showNewVal (void) {
  //LCD_rect(198, 0, 55, 120, bgColour);
  drawStr(0, 150, WHITE, scale, "New:", 4);
  drawInt(3 * scale * 8, 150, RED, scale, newVal, 4);
}


void checkLim (void) {
  if (diff + oldVal > settingMaxs[valToChange]) {
    diff = settingMaxs[valToChange] - oldVal;
  } else if (oldVal + diff < settingMins[valToChange]) {
    diff = settingMins[valToChange] - oldVal;
  }
}

void readAllparams ( void ) {
  curDelay = readParam('d');
  delay(100);
  clearSerial();
  delay(100);
  curWatchdog = readParam('w');
  delay(100);
  clearSerial();
  delay(100);
  curPressIgnore = (bool) readParam('i');
  delay(100);
  clearSerial();

}

void showReadScreen ( void ) {
  settingScreenText();
  settingScreenArrow();
}

//col,int16_t row, int16_t width, int16_t height, int16_t color
void settingScreenArrow( void ) {
  LCD_rect(198, 0, 55, 120, bgColour);
  LCD_rect(200, 10 + 10 * scale * valToChange, 50, 5, WHITE);
  LCD_rect(198, 12 + 10 * scale * valToChange, 2, 1, WHITE);
  LCD_rect(202, 7 + 10 * scale * valToChange, 2, 11, WHITE);
}


void settingScreenText( void ) {


  drawStr(0, 10 * scale * del, MAGENTA, scale, "Delay:", 6);
  drawInt(4 * scale * 8, 10 * scale * del, MAGENTA, scale, curDelay, 4);

  drawStr(0, 10 * scale * wdog, GREEN, 3, "WDOG:", 5);
  drawInt(4 * scale * 8, 10 * scale * wdog, GREEN, scale, curWatchdog, 4);

  drawStr(0, 10 * scale * pig, CYAN, 3, "p ig:", 5);
  drawInt(4 * scale * 8, 10 * scale * pig, CYAN, scale, curPressIgnore, 1);
}
