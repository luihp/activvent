byte curLims[4] = {0, 0, 0, 0};
const byte limitMin = 0;
const byte limitMax = 180;
const byte limitButtonDiff = 5;
const byte limitPotSpan = 10;
const char limitChar[4] = {'a', 'b', 'c', 'd'};

byte limSel = 0;
byte prevLimSel = 0;

int diffLim = 0;
int newLimprePot = 0;
int newLim = 0;

enum LimitSelected {
  a,
  b,
  c,
  d
};




void initScreenLimits (bool stopReading) {
  bgColour = BLUE;
  
  LCD_clear(bgColour);

  if (stopReading) {
    setDataStream ( false );
    drawStr(0, 10 * scale * a, MAGENTA, scale, "Stopping data", 13);
    delay(2000);
  }
  drawStr(0, 10 * scale * b, MAGENTA, scale, "Reading data", 12);

  clearSerial();
  readAllLimits();

  LCD_clear(bgColour);
  prevLimSel = -1;

  //mode = settings * numSubStates + choosing;
  showLimitScreen();
}


void limitsLoop ( void ) {
  switch ( mode % numSubStates ) {
    
    case choosing:

      limSel = analogRead(A7) / 256;

      if (!digitalRead(button2Pin) &&
          millis() - tLastPressed > tDeadband) {
        tLastPressed = millis();

        mode = limits * numSubStates + changing;

        potVal = analogRead(A7);
        diffLim = 0;
        
        newLimprePot = curLims[limSel];
        newLim = newLimprePot + (potVal * limitPotSpan / 1023) - (limitPotSpan/2);

        showNewLim();

        drawStr(0, 180, WHITE, scale, "b1:- b3:+ pot:fine", 5);
        //drawStr(scale * 5 * 5, 180, BLUE, scale, "b1: -", 5);
        drawStr(0, 210, BLUE, scale, "b2:sel", 9);
      }
      else if (limSel != prevLimSel) {
        limitScreenArrow();
        prevLimSel = limSel;

      }
      break;

    case changing:

      potVal = analogRead(A7);
      //newLim = newLimprePot + (potval * limitPotSpan / 1023) - (limitPotSpan/2);

      if (!digitalRead(button1Pin) &&
          millis() - tLastPressed > tDeadband) {
        tLastPressed = millis();
        diffLim -= limitButtonDiff;
        checkLimLim();

        newLimprePot = curLims[limSel] + diffLim;
        newLim = newLimprePot + (potVal * limitPotSpan / 1023) - (limitPotSpan/2);

        showNewLim();
      } else if (!digitalRead(button3Pin) &&
          millis() - tLastPressed > tDeadband) {
        tLastPressed = millis();
        diffLim += limitButtonDiff;
        checkLimLim();

        newLimprePot = curLims[limSel] + diffLim;
        newLim = newLimprePot + (potVal * limitPotSpan / 1023) - (limitPotSpan/2);

        showNewLim();
      }

      if (newLim - (newLimprePot + (potVal * limitPotSpan / 1023) - (limitPotSpan/2)) != 0) {
        newLim = newLimprePot + (potVal * limitPotSpan / 1023) - (limitPotSpan/2);
        showNewLim();
      }

      
      
      if (!digitalRead(button2Pin) &&
          millis() - tLastPressed > tDeadband) {
        tLastPressed = millis();
        mode = limits * numSubStates + confirming;

        drawStr(0, 180, WHITE, scale, "b2: CONFIRM", 11);
        drawStr(0, 210, WHITE, scale, "b3: cancel", 10);
      }
      break;

    case confirming:


      if (!digitalRead(button2Pin) &&
          millis() - tLastPressed > tDeadband + 500) {
        tLastPressed = millis();
        drawStr(0, 120, RED, scale, "SENDING", 7);
        setLimit(limitChar[limSel], newLim);

        delay(2000);

        initScreenLimits(false);

        mode = limits * numSubStates + choosing;
      }

      if (!digitalRead(button3Pin) &&
          millis() - tLastPressed > tDeadband) {
        tLastPressed = millis();

        delay(50);
        mode = settings * numSubStates + choosing;
        initSettingsMode(false);
      }

      break;
  }

  while (millis() - tStart < tLoop) {
    ;
  }
}

void showNewLim (void) {
  LCD_rect(3 * scale * 8, 0, 20, 25, bgColour);

  
  drawStr(0, 150, WHITE, scale, "New:", 4);
  drawInt(3 * scale * 8, 150, RED, scale, newLim, 4);
}


void checkLimLim (void) {
  if (diffLim + curLims[limSel] > limitMax) {
    diffLim = limitMax - curLims[limSel];
  } else if (curLims[limSel] + diffLim < limitMin) {
    diffLim = limitMin - curLims[limSel];
  }
}


void showLimitScreen ( void ) {
  limitScreenText();
  limitScreenArrow();
}

//col,int16_t row, int16_t width, int16_t height, int16_t color
void limitScreenArrow( void ) {
  LCD_rect(198, 0, 55, 120, bgColour);
  LCD_rect(200, 10 + 10 * scale * limSel, 50, 5, WHITE);
  LCD_rect(198, 12 + 10 * scale * limSel, 2, 1, WHITE);
  LCD_rect(202, 7 + 10 * scale * limSel, 2, 11, WHITE);
}


void limitScreenText( void ) {


  drawStr(0, 10 * scale * a, MAGENTA, scale, "A:", 2);
  drawInt(4 * scale * 8, 10 * scale * a, MAGENTA, scale, curLims[a], 4);

  drawStr(0, 10 * scale * b, GREEN, 3, "B:", 2);
  drawInt(4 * scale * 8, 10 * scale * b, GREEN, scale, curLims[b], 4);

  drawStr(0, 10 * scale * c, RED, 3, "C:", 2);
  drawInt(4 * scale * 8, 10 * scale * c, RED, scale, curLims[c], 1);

  drawStr(0, 10 * scale * d, WHITE, 3, "D:", 2);
  drawInt(4 * scale * 8, 10 * scale * d, WHITE, scale, curLims[d], 1);
}

void readAllLimits ( void ) {
  curLims[a] = readLimit('a');
  delay(100);
  clearSerial();
  delay(500);
  curLims[b] = readLimit('b');
  delay(100);
  clearSerial();
  delay(500);
  curLims[c] = readLimit('c');
  delay(100);
  clearSerial();
  delay(500);
  curLims[d] = readLimit('d');
  delay(100);
  clearSerial();

}
