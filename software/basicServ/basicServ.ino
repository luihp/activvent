

#include <Servo.h>


Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo


int potpin = A7 ;  // analog pin used to connect the potentiometer
int val;    // variable to read the value from the analog pin

void setup() {
  myservo1.attach(5);  // attaches the servo on pin 9 to the servo object
  myservo2.attach(6);  // attaches the servo on pin 9 to the servo object
  Serial.begin(115200);
  delay(50);
}


long loopTime = 20;
long waitTime = -1;
long tTime = 0;

long timeNow = micros();

void loop() {
  val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
  val = map(val, 0, 1023, 0, 180);     // scale it to use it with the servo (value between 0 and 180)
  myservo1.write(val);                  // sets the servo position according to the scaled value
  myservo2.write(val); // About 100uS
  Serial.println(val);

  tTime = micros() - timeNow;
  while(micros() - timeNow < loopTime * 1000) {
    //delayMicroseconds(10);
  }
  timeNow = micros();
}


void dump(int pinNum) {

  //analogRead(pinNum);
  Serial.print(analogRead(pinNum));
  Serial.print(',');

}
