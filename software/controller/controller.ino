/*
    It is of import to note that I am going to assume that the device is going to be functional
    or else reset prior to the millis rollover time.
*/

#include <Servo.h>
#include <CircularBuffer.h>

#define LOPENINIT 113
#define LCLOSEINIT 10
#define RCLOSEINIT 7
#define ROPENINIT 113

#define QTAR 200
#define QTOL 50
#define QGRADTAR 25
#define QGRADTOL 10

#define PTAR 100
#define PTOL 10

#define LOOPPRINTFREQ 10
#define MINFLATTIME 150

#define INITLOOPPERIOD 2500 // 24 BPM total (12 each)
#define TIMETOSWITCH 550

#define GRADSAMPNUM 5

bool pIgnore = true;
bool qControlEnable = true;
bool fControlEnable = false;
bool haveFSwitched = false;
bool haveQswitched = false;
bool serialWriteEnable = true;
//bool serialWriteEnable = false; //Needs to be true to enable the screen

enum State {
  up,
  down,
  flat
};

enum Sensor {
  qt,
  ql,
  qr,
  qe,
  pt,
  pl,
  pr,
  pe
};

enum BreathPart {
  br,
  in,
  delta
};

// Time-based variables for pace setting etc.
unsigned long tLoop = 20;
unsigned long tStart = 0; // Start of loop time
unsigned long tLoopDur = 0;
unsigned long loopC = 0;
unsigned long tDeficit = 0;


// Time-based variables for
unsigned long tFlat = 0;
unsigned long tRise = 0;
unsigned long tSwitch = 0;
unsigned long tLabrador = 5000; // Watchdog timer for last switching time in ms
unsigned long runningF = INITLOOPPERIOD;
unsigned long cT = 0;
unsigned long lastPrintTime = 0;
const unsigned long tServoTuneMax = 20000;
int tFlatPause = 250;


uint8_t delayRatio = 3;

int lClose = LCLOSEINIT;
int lOpen = LOPENINIT;
int rClose = RCLOSEINIT;
int rOpen = ROPENINIT;


//unsigned long tStart = 0;
//unsigned long tEnd = 0;


int curS = flat;
char printCmd = 'n';

int loopPrintFreq = 10;
int loopCalcFreq = 10;
int loopWriteFreq = 2;

int printVal = 0;
byte printByte = 0;

uint8_t graphingSource = 0;


void setup() {
  setupIO();
}


void(* resetFunc) (void) = 0;

void loop() {
  tStart = millis();
  setLED1(tLoopDur > tLoop); // Turns LED1 on if loop time last tim was too slow
  tDeficit = (tLoopDur > tLoop) ? tLoopDur - tLoop : 0 ; // Calc how far behind we are
  //tDeficit = 0;

  // Re read an recalculate all values (I should time this)
  quickReadAll();
  reCalcAll();





  if (curS == flat &&
      (getGrad(qt) > QGRADTAR) &&
      (millis() - tFlat > MINFLATTIME) ) {

    tRise = millis();
    curS = up;
    setLED2(HIGH);

    haveQswitched = false;

  }

  else if (curS == up && getGrad(qt) < - QGRADTAR) {
    curS = down;
  }

  else if (curS == down &&
           (abs(getMean(qt) - QTAR) < QTOL) && // If Q is zero
           (abs(getGrad(qt)) < QGRADTOL) &&
           ((abs(getMean(pt) - PTAR) < PTOL) or pIgnore) ) { // And unchanging

    tFlat = millis();
    if (serialWriteEnable) {
      Serial.write('f');
    }

    curS = flat;
    setLED2(LOW);
  }

  else if (curS == flat &&
           qControlEnable &&
           !haveQswitched &&
           (millis() - tFlat > tFlatPause)) {
    change();
    tSwitch = millis();
    haveQswitched = true;
  }

  else if (tLabrador && millis() - tSwitch > tLabrador) {
    //alarm();
    change();
    tSwitch = millis();
    setLED3(HIGH);
  }



  // I need to check that this is all positive: (getMeanTiming(delta) - TIMETOSWITCH)





  printInfo(loopC % loopPrintFreq, printCmd); // Only prints if the modResult == 0


  if (serialWriteEnable &&
      (loopC % loopWriteFreq) == loopWriteFreq - 1) {
    
    printVal = getMean(graphingSource + 4);
    Serial.write('d');

    Serial.write( printVal / 256);
    Serial.write( printVal % 256);

    printVal = getMean(graphingSource);
    Serial.write( printVal / 256);
    Serial.write( printVal % 256);
  }




  if ((loopC % 100) == 5) {
    if (Serial.available() > 0) {
      parseInput();
    }
  }

  tLoopDur = millis() - tStart;
  while ( millis() - tStart < (tLoop - tDeficit)) {
    tDeficit = 0;
  }
  loopC++;

}

void parseInput ( void ) {
  char charIn = Serial.read();
  //Serial.print(F("Got command: "));
  //Serial.println(charIn);

  switch (charIn) {
    case 't':
      tuneServo();
      break;
    case 'l':
      switch (Serial.read()) {
        case 'a': //lc
          if (Serial.available()) {
            lClose = getNum();
            Serial.print(F("Setting la = "));
            Serial.println(lClose);
          } else {
            Serial.print(lClose);
          }
          break;
        case 'b':
          if (Serial.available()) {
            lOpen = getNum();
            Serial.print(F("Setting lb = "));
            Serial.println(lOpen);
          } else {
            Serial.print(lOpen);
          }
          break;
        case 'c':
          if (Serial.available()) {
            rClose = getNum();
            Serial.print(F("Setting lc = "));
            Serial.println(rClose);
          } else {
            Serial.print(rClose);
          }
          break;
        case 'd':
          if (Serial.available()) {
            lClose = getNum();
            Serial.print(F("Setting ld = "));
            Serial.println(rOpen);
          } else {
            Serial.print(rOpen);
          }
          break;
      }
      break;
    case 'f':
      tLoop = (long) getNum();
      Serial.print(F("Setting loop time = "));
      Serial.println(tLoop, DEC);
      break;
    case 'p':
      loopPrintFreq = getNum();
      Serial.print(F("Setting print period = "));
      Serial.println(loopPrintFreq, DEC);
      break;
    case 'i':
      pIgnore = (bool) getNum();
      Serial.print(F("Setting pressure ignore = "));
      Serial.println(pIgnore, DEC);
      break;
    case 's':
      serialWriteEnable = (bool) getNum();
      Serial.print(F("Setting serial write = "));
      Serial.println(pIgnore);
      break;
    case 'w':
      tLabrador = getNum();
      Serial.print(F("Setting watchdog timer = "));
      Serial.println(tLabrador, DEC);
      break;
    case 'r':
      resetFunc();
      break;
    case 'd':
      tFlatPause = getNum();
      Serial.print(F("Setting flat delay time :"));
      Serial.println( tFlatPause);

    case 'g':
      graphingSource = getNum() % 4;      
      break;
    case 'q':
      delay(1);
      if (Serial.available() > 0) {
        query(Serial.read());
        }
      
      break;
    case 'o':
      if (Serial.available() > 0) {
        printCmd = Serial.read();
        Serial.print(F("Setting print command: "));
        Serial.println(printCmd);
      }
      break;
    case '?':
      printHelp();
      printCmd = 'n';
    // I intentionally have no break here
    // so that in the case of the ? it stops printing
    // so you can actually read it.
    default:
      printCmd = charIn;
      break;
  }
  //Serial.read();
}


/*   
 *    
  Serial.println(F("[q]uery; (e.g. 'qd') request one of the following values:"));
  Serial.println(F("[d]elay"));
  Serial.println(F("[w]atchdog"));
  Serial.println(F("[i]gnore pressure state"));
  Serial.println(F("[p]rint period"));

  */

void query ( char cmd ) {
  switch(cmd) {
    case 'd':
      // int
      Serial.print(tFlatPause);
      break;
    case 'w':
      // unsigned long
      Serial.print(tLabrador);
      break;
    case 'i':
      // bool
      Serial.print(pIgnore);
      break;
    case 'p':
      Serial.print(loopPrintFreq);
      break;
    case 'f':
      Serial.print(tLoop);
      break;
    case 'g':
      Serial.print(graphingSource);
  }
}


void printInfo (int modResult, char cmd) {

  if (modResult == 0) {
    switch (printCmd) {
      case 'b': // [b]rief
        printAll();
        break;
      case 'v': // [v]erbose
        printAllVerbose();
        break;
      case 'l': // [l]ong
        printAll();
        printAllVerbose();
        break;
      case 'q': // flow
        Serial.print(getMean(qt));
        Serial.print(F(", "));
        Serial.println(getGrad(qt));
        break;
      case 'c': // [c]ompact
        cT = millis();
        Serial.print(cT - lastPrintTime);
        Serial.print(F(","));
        Serial.print(getMean(qt));
        Serial.print(F(","));
        Serial.println(getMean(pt));
        lastPrintTime = cT;

        break;
      case 'd': // [d]ebug
        cT = millis();
        Serial.println(cT - lastPrintTime);
        printqdata();
        lastPrintTime = cT;
        break;
      default:
        ;
    }
  }
  else if (modResult == 1 &&
           printCmd == 'd') {
    printpdata();
    Serial.println();
  }
}
