/*
   I am going to draw a picture of the system:
            |
            ^QE/PE
            |
            ⅄ - Passive Y
           | |
          |   |
          |   |
     PL/QL^   ^QR/PR
          |   |
          |   |
          |   |
           X X
            Y Active switching valve
            |
            ^QI/PI
            |
            |
            |
*/



#define QTPIN A0
#define QLPIN A5
#define QRPIN A6
#define QEPIN A2
#define PTPIN A1
#define PLPIN A4
#define PRPIN A7
#define PEPIN A3
#define POTPIN A7

#define SLPIN 5
#define SRPIN 6

#define LED0PIN 13
#define LED1PIN 8
#define LED2PIN 4
#define LED3PIN 7

#define BUTTON1PIN 2
#define BUTTON2PIN 3

#define BUFFLENGTH 10


Servo servR;
Servo servL;

CircularBuffer<int, BUFFLENGTH> qTbuff;
CircularBuffer<int, BUFFLENGTH> qLbuff;
CircularBuffer<int, BUFFLENGTH> qRbuff;
CircularBuffer<int, BUFFLENGTH> qEbuff;

CircularBuffer<int, BUFFLENGTH> pTbuff;
CircularBuffer<int, BUFFLENGTH> pLbuff;
CircularBuffer<int, BUFFLENGTH> pRbuff;
CircularBuffer<int, BUFFLENGTH> pEbuff;

bool outState = false;

int stat[8][3];
int peepEst = 0;

void setupIO( void ) {
  servL.attach(SLPIN);
  servR.attach(SRPIN);
  Serial.begin(115200);
  delay(50);

  pinMode(QTPIN, INPUT);
  pinMode(QLPIN, INPUT);
  pinMode(QRPIN, INPUT);
  pinMode(QEPIN, INPUT);
  pinMode(PTPIN, INPUT);
  pinMode(PLPIN, INPUT);
  pinMode(PRPIN, INPUT);
  pinMode(PEPIN, INPUT);
  pinMode(POTPIN, INPUT);

  pinMode(LED0PIN, OUTPUT);
  pinMode(LED1PIN, OUTPUT);
  pinMode(LED2PIN, OUTPUT);
  pinMode(LED3PIN, OUTPUT);

  initBuff(qTbuff, QTPIN, qt);
  initBuff(qLbuff, QLPIN, ql);
  initBuff(qRbuff, QRPIN, qr);
  initBuff(qEbuff, QEPIN, qe);
  initBuff(pTbuff, PTPIN, pt);
  initBuff(pLbuff, PLPIN, pl);
  initBuff(pRbuff, PRPIN, pr);
  initBuff(pEbuff, PEPIN, pe);


}

void change (void) {
  if (serialWriteEnable) {
    Serial.write('c');
  }
  setLED0(outState);
  if (outState) {
    servL.write(lOpen);
    servR.write(rClose);
  } else {
    servL.write(lClose);
    servR.write(rOpen);
  }

  outState = not outState;
}

void tuneServo( void ) {
  unsigned long tBegin = millis();
  Serial.println(F("Taking control of servo!!"));
  while ( millis() - tBegin < tServoTuneMax &&
          !readButton1()) {
    int val = map(analogRead(POTPIN), 0, 1023, 0, 180);
    Serial.println(val);
    setBothServo(val);
    delay(20);
  }
  change();
}

void setBothServo( int val ) {
  servL.write(val);
  servR.write(val);
}

void setLED0 ( bool state ) {
  digitalWrite(13, state);
}

void setLED1 ( bool state ) {
  digitalWrite(LED1PIN, state);
}

void setLED2 ( bool state ) {
  digitalWrite(LED2PIN, state);
}

void setLED3 ( bool state ) {
  digitalWrite(LED3PIN, state);
}

bool readButton1 ( void ) {
  return digitalRead(BUTTON1PIN);
}

bool readButton2 ( void ) {
  return digitalRead(BUTTON2PIN);
}

void readAll (void) {
  readSensor(qTbuff, QTPIN, qt);
  readSensor(qLbuff, QLPIN, ql);
  readSensor(qRbuff, QRPIN, qr);
  readSensor(qEbuff, QEPIN, qe);
  readSensor(pTbuff, PTPIN, pt);
  readSensor(pLbuff, PLPIN, pl);
  readSensor(pRbuff, PRPIN, pr);
  readSensor(pEbuff, PEPIN, pe);
}

void quickReadAll ( void ) {
  quickReadSensor(qTbuff, QTPIN);
  quickReadSensor(qLbuff, QLPIN);
  quickReadSensor(qRbuff, QRPIN);
  quickReadSensor(qEbuff, QEPIN);
  quickReadSensor(pTbuff, PTPIN);
  quickReadSensor(pLbuff, PLPIN);
  quickReadSensor(pRbuff, PRPIN);
  quickReadSensor(pEbuff, PEPIN);
}


void initBuff(CircularBuffer<int, BUFFLENGTH> &buff, int pinIn, int key) {
  while (buff.push(analogRead(pinIn))) {
    delay(5);
  }
  calcBuff(buff, key);
  //return calcBuff(buff);
}

void reCalcAll( void ) {
  calcBuff(qTbuff, qt);
  calcBuff(qLbuff, ql);
  calcBuff(qRbuff, qr);
  calcBuff(qEbuff, qe);
  calcBuff(pTbuff, pt);
  calcBuff(pLbuff, pl);
  calcBuff(pRbuff, pr);
  calcBuff(pEbuff, pe);
}

void readSensor(CircularBuffer<int, BUFFLENGTH> &buff, int pinIn, int key) {
  int val = analogRead(pinIn);
  stat[key][1] -= buff.first();
  stat[key][1] += val;
  buff.push(val);

  int mn = stat[key][0];
  int mx = stat[key][2];

  stat[key][0] = (val < mn) ? val : mn;
  stat[key][2] = (val > mx) ? val : mx;

  //return stats;
}

int getGrad( int key ) {
  switch (key) {
    case qt:
      return calcGrad(qTbuff);
      break;
    case ql:
      return calcGrad(qLbuff);
      break;
    case qr:
      return calcGrad(qRbuff);
      break;
    case pt:
      return calcGrad(pTbuff);
      break;
    case pl:
      return calcGrad(pLbuff);
      break;
    case pr:
      return calcGrad(pRbuff);
      break;
    case qe:
      return calcGrad(qEbuff);
      break;
    case pe:
      return calcGrad(pEbuff);
      break;
    default:
      break;
  }
}

int calcGrad(CircularBuffer<int, BUFFLENGTH> &buff) {
  int nSegs = (BUFFLENGTH / GRADSAMPNUM);
  int rAvg[nSegs];
  memset(rAvg, 0, sizeof(rAvg));
  int grad[nSegs - 1];


  for (int i = 0; i < nSegs; i++) {
    for (int ii = 0; ii < GRADSAMPNUM; ii++) {
      rAvg[i] += buff[i * GRADSAMPNUM + ii];
    }
  }

  int maxG = rAvg[1] - rAvg[0];
  int indG = 0;
  for (int i = 0; i < nSegs - 1 ; i++) {
    grad[i] = rAvg[i + 1] - rAvg[i];

    if (abs(grad[i]) > abs(maxG)) {
      maxG = grad[i];
      indG = i;
    }

  }

  return maxG;
}

void quickReadSensor(CircularBuffer<int, BUFFLENGTH> &buff, int pinIn) {
  /* Pretty much the same, but it won't update stat */
  buff.push(analogRead(pinIn));
}

int getMean (int key) {
  return (stat[key][1] / BUFFLENGTH);
}

int getVar (int key) {
  return (stat[key][2] - stat[key][0]);
}

void calcBuff(CircularBuffer<int, BUFFLENGTH> &buff, int key) {
  int sum = 0;
  int mn = buff[0];
  int mx = buff[0];
  for (int i = 0; i < BUFFLENGTH; i++ ) {
    sum += buff[i];
    mn = (buff[i] < mn) ? buff[i] : mn;
    mx = (buff[i] > mx) ? buff[i] : mx;
  }


  stat[key][0] = mn;
  stat[key][1] = sum;
  stat[key][2] = mx;
  //return stats;
}



int getNum( void ) {
  int sum = 0;
  int in = 0;
  while (Serial.available() > 0) {
    in = Serial.read() - 48;
    if (in >= 0 && in < 10) {
      sum *= 10;
      sum += in;
    }
  }
  return sum;
}




void printAllVerbose( void ) {
  //Serial.println("Printing all verbose");
  printAllBuff();
}

void printAllBuff( void ) {
  printBuff(qTbuff, qt);
  printBuff(qLbuff, ql);
  printBuff(qRbuff, qr);
  printBuff(pTbuff, pt);
  printBuff(pLbuff, pl);
  printBuff(pRbuff, pr);
}

void printqdata ( void ) {
  printBuff_short(qTbuff, qt);
  printBuff_short(qLbuff, ql);
  printBuff_short(qRbuff, qr);
  printBuff_short(qEbuff, qe);
}

void printpdata ( void ) {
  printBuff_short(pTbuff, pt);
  printBuff_short(pLbuff, pl);
  printBuff_short(pRbuff, pr);
  printBuff_short(pEbuff, pe);
}

void printAllData ( void ) {
  printqdata();
  printpdata();
}

void printTotals( void ) {
  printBuff_short(qTbuff, qt);
  printBuff_short(pTbuff, pt);
}

void printBuff (CircularBuffer<int, BUFFLENGTH> &buff, int key) {
  Serial.print(key);
  Serial.println(":");
  Serial.print("[ ");
  for (int i = 0; i < BUFFLENGTH; i++ ) {
    Serial.print(buff[i]);
    Serial.print(", ");
  }
  Serial.println("]");
  Serial.print("[ ");
  Serial.print(stat[key][0]);
  Serial.print(", ");
  Serial.print(stat[key][1]);
  Serial.print(", ");
  Serial.print(stat[key][2]);
  Serial.println("]");
  Serial.print(getMean(key));
  Serial.print(", ");
  Serial.print(getVar(key));
  Serial.println();
}

void printBuff_short ( CircularBuffer<int, BUFFLENGTH> &buff, int key ) {
  Serial.print(key);
  Serial.print(F(":\n["));
  for (int i = 0; i < BUFFLENGTH; i++ ) {
    Serial.print(buff[i]);
    Serial.print(",");
  }
  Serial.println("]");
}

void printHelp ( void ) {

  Serial.println(F("The following commands are available:"));
  Serial.println(F("[f]req: Set the goal loop period, e.g. 'f5' would set it to 5 mS."));
  Serial.println(F("[p]rint period: Set the print period, in multiples of the loop period, e.g. 'p10'."));
  Serial.println(F("[i]gnore pressure: Specify whether pressure must reach baseline to switch or not."));
  Serial.println(F("        Set to 0 or 1, e.g. i1 to ignore (e.g. if PEEP is used)"));
  Serial.println(F("[w]atchdog: Set the maximum time (in ms) at which point it will automatically switch"));
  Serial.println(F("[s]erial writing for screen, same as pressure ignore setting"));
  Serial.println(F("[g]raphing sensor source: 0=Insp, 1=L, 2=R, 3=Exp e.g. g0 for inspiratory"));
  Serial.println(F("[d]elay: set the time (in ms) after the end of inspiration that switching is triggered`"));
  Serial.println(F("[l]imit: set the limit for the servo travel. a = LC, b = LO, c = RC, d = RO"));
  Serial.println(F("For example: lc90"));
  Serial.println(F("[t]une servos; THIS STOPS THE CONTROL SYSTEM FROM FUNCTIONING!!"));
  Serial.println();
  Serial.println(F("[q]uery; (e.g. 'qd') request one of the following values:"));
  Serial.println(F("[d]elay"));
  Serial.println(F("[w]atchdog"));
  Serial.println(F("[i]gnore pressure state"));
  Serial.println(F("[p]rint period"));
  Serial.println(F("[f]req"));
  Serial.println();
  Serial.println(F("Print/[o]utput Settings (e.g. 'ob'):"));
  Serial.println(F("[b]asic: Only print last, mean, and var of each sensor."));
  Serial.println(F("[v]erbose: Print full buffers, and min, mean, and max."));
  Serial.println(F("[d]ata: prints data without meta"));
  Serial.println(F("[l]ong: Print both basic and verbose."));
  Serial.println(F("[n]one"));
  Serial.println();
  Serial.println(F("[?] help: Print this message, and set print settings to [n]one."));
}


void printAll( void ) {

  static char s[30];
  Serial.println(F("  QT,  QL,  QR,  QE,  PT,  PL,  PR,  PE"));
  sprintf(s, "%4i,%4i,%4i,%4i,%4i,%4i,%4i,%4i", qTbuff.last(), qLbuff.last(), qRbuff.last(), qEbuff.last(), pTbuff.last(), pLbuff.last(), pRbuff.last(), pEbuff.last());
  Serial.println(s);

  sprintf(s, "%4i,%4i,%4i,%4i,%4i,%4i,%4i,%4i,%4i,%4i", getMean(qt), getMean(ql), getMean(qr), getMean(qe), getMean(pt), getMean(pl), getMean(pr), getMean(pe));
  Serial.println(s);

  sprintf(s, "%4i,%4i,%4i,%4i,%4i,%4i\n", getVar(qt), getVar(ql), getVar(qr), getVar(qe), getVar(pt), getVar(pl), getVar(pr), getVar(pe));
  Serial.println(s);
}
